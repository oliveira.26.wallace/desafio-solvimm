/* eslint-disable linebreak-style */
/* eslint-disable no-console */
// eslint-disable-next-line import/no-unresolved
const AWS = require('aws-sdk');
const { basename, extname, dirname } = require('path');
const sizeOf = require('image-size');// Pacote para obter as dimensões e o tipo da imagens

const dynamoDb = new AWS.DynamoDB.DocumentClient();
const S3 = new AWS.S3();

/**
 * Função para extrair a dimensão e o tipo das Imagem que foi feitas Uploads,
 *  coloca essas informações em uma tabela no dynamodb
 *
 * @params upload de arquivo(s) no bucket serverless-wallace na pasta uploads
 *
 * @returns sucesso -> Insere os dados no dynamoDb e informa no console
 *                      que o cadastro foi um sucesso
 *          erro -> retorna uma mensagem no console com
 *                  o erro e o nome do arquivo que ocorreu o erro;
 */

// eslint-disable-next-line import/prefer-default-export
module.exports.handle = async ({ Records: uploads }) => {
  await Promise.all(uploads.map(async (upload) => { // Para cada Imagem anexada faça
    const { key } = upload.s3.object;
    const params = {
      Bucket: process.env.bucket, // Nome do bucket
      Key: key,
    };
    const imagem = await S3.getObject(params).promise();
    const dimensions = sizeOf(imagem.Body);
    const s3objectkey = `${basename(key, extname(key))}`; // Apenas o nome do arquivo
    const dir = `${dirname(key)}`; // Diretorio do arquivo
    const item = {
      TableName: process.env.table, // Nome da tabela
      Item: {
        s3objectkey,
        height: dimensions.width,
        width: dimensions.height,
        type: dimensions.type,
        size: imagem.ContentLength,
        dir,
      },
    };
    await dynamoDb.put(item).promise()
      .then((result) => console.log(`Cadastro realizado!\nResultado: ${result}`))
      .catch((err) => console.log(`Ocorrou um erro no arquivo:${key}.\nErro: ${err}`));
  }));
};

/* eslint-disable linebreak-style */
/* eslint-disable no-console */
// eslint-disable-next-line import/no-unresolved
const AWS = require('aws-sdk');
const { basename, extname } = require('path');

const dynamoDb = new AWS.DynamoDB.DocumentClient();
const S3 = new AWS.S3();

/**
 * Função para baixar(obter as Imagens)
 *
 * @Rota  Get - images/{s3objectkey}
 *
 * @param s3objectkey -> Informe o nome da imagem
 *
 * @returns sucesso -> retorna um Json com a imagem solicitada;
 *          erro -> retorna uma mensagem na requisição e informa no console qual foi o erro
 */

// eslint-disable-next-line import/prefer-default-export
module.exports.handle = async (event, context, callback) => {
  let { s3objectkey } = event.pathParameters;
  // Vê qual a extensão que foi passada no parametro
  s3objectkey = `${basename(s3objectkey, extname(s3objectkey))}`;
  // Parametros para buscar no DynamoDb
  let params = {
    TableName: process.env.table,
    Key: { s3objectkey },
  };
  await dynamoDb.get(params).promise()
    .then((result) => {
      // Insere o diretorio, o nome e a extensão do arquivo para realizar a busca no s3
      s3objectkey = `${result.Item.dir}/${s3objectkey}.${result.Item.type}`;
    })
    .catch((err) => {
      console.log('Error');
      console.log(err);
      callback(new (Error('Ocorreu um erro na requisicao'))());
    });
  // Parametros para buscar no S3
  params = {
    Bucket: process.env.bucket,
    Key: `${s3objectkey}`,
  };
  await S3.getObject(params).promise()
    .then((result) => {
      const res = {
        statusCode: 200,
        body: JSON.stringify(result),
      };
      callback(null, res);
    })
    .catch((err) => {
      console.log('Error');
      console.log(err);
      callback(new (Error('Ocorreu um erro na requisicao'))());
    });
};

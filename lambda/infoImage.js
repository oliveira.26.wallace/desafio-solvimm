/* eslint-disable linebreak-style */
/* eslint-disable no-console */
// eslint-disable-next-line import/no-unresolved
const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

/**
 * Função que retorna a maior Imagem, menor Imagem, quais as extensões e quantidade de
 * imagem em cada extensão
 *
 * @Rota  Get - /metadata
 *
 * @returns sucesso -> um Json com:
 *              maiorImagem: Objeto contendo os metaDados da maior imagem,
 *              menorImagem: Objeto contendo os metaDados da menor imagem,
 *              tipoImagems: Um array de Objetos contendo:
 *                      type: tipo de Imagem,
 *                      total: total de imagem com o mesmo tipo
 *
 *           erro -> Uma messagem informando que não há arquivos no banco
 */

// eslint-disable-next-line import/prefer-default-export
module.exports.handle = async (event, context, callback) => {
  const params = {
    TableName: process.env.table,
  };
  const resp = await dynamoDb.scan(params).promise();
  const items = resp.Items;
  if (items) {
    // Objeto que Server para armazenar as Informações daquele que tem o maior tamanho
    let maiorImagem = {};
    // Objeto que para armazenar as Informações daquele que tem o maior tamanho
    let menorImagem = {};
    // Array que Server para nome e a quantidade de cada tipo diferente de Imagem
    const tipoImagems = [];
    // eslint-disable-next-line array-callback-return
    items.map((item) => {
      if (!maiorImagem.size) {
        // Caso o maiorImagem está vazio quer dizer que todos os outros items
        // estão vazio então insere o primeiro item dentro de todas as variaveis
        maiorImagem = item;
        menorImagem = item;
        tipoImagems.push({
          type: item.type,
          total: 1,
        });
        return;
      }
      if (item.size > maiorImagem.size) {
        maiorImagem = item;
      }
      if (item.size < menorImagem.size) {
        menorImagem = item;
      }
      // Retorna o index no array tipoImagens,caso tenha um arquivo com o mesmo tipo que o item.
      // Se não tiver ele retorna -1
      const verificacao = tipoImagems.findIndex((imagem) => imagem.type === item.type);
      if (verificacao !== -1) {
        tipoImagems[verificacao].total += 1;
      } else {
        // insere as informações no array caso o retorno do index for -1
        tipoImagems.push({
          type: item.type,
          total: 1,
        });
      }
    });
    const res = {
      statusCode: 200,
      body: JSON.stringify({
        maiorImagem,
        menorImagem,
        tipoImagems,
      }),
    };
    callback(null, res);
  } else {
    const res = {
      message: 'Não há arquivos no banco',
      body: JSON.stringify({ }),
    };
    callback(null, res);
  }
};

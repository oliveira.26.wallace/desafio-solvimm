/* eslint-disable linebreak-style */
/* eslint-disable import/no-unresolved */
/* eslint-disable no-console */
const AWS = require('aws-sdk');
const { basename, extname } = require('path');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

/**
 * Função que retorna a dimensão, tamanho e o tipo da imagem solicitada
 *
 * @Rota  Get - /metadata/{s3objectkey}
 *
 * @param s3objectkey -> Informe o nome da imagem
 *
 * @returns sucesso -> retorna um Json com a altura, largura , tamanho e o tipo da imagem solicitada
 *          erro -> retorna uma mensagem que ocorreu um erro na requisição
 *                  e informa no console qual foi o erro e qual o nome do arquivo que ocorreu o erro
 */
// eslint-disable-next-line import/prefer-default-export
module.exports.handle = async (event, context, callback) => {
  let { s3objectkey } = event.pathParameters;
  s3objectkey = `${basename(s3objectkey, extname(s3objectkey))}`;// Para caso for informado qual a extensão do arquivo
  const params = {
    // Nome tabela
    TableName: process.env.table,
    Key: { s3objectkey },
  };
  await dynamoDb.get(params).promise()
    .then((result) => {
      const res = {
        statusCode: 200,
        body: JSON.stringify(result.Item),
      };
      callback(null, res);
    })
    .catch((err) => {
      console.log(`Ocorrou um erro no na busca do :${params.Key}.\nErro: ${err}`);
      callback(new (Error('Ocorreu um erro na requisicao'))());
    });
};
